import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginCode, VerifyCodeResponse } from 'src/app/interfaces/login.interface';
import { LoginService } from 'src/app/services/login.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  forma!:FormGroup; // Reactive form
  show:boolean = false; // Servirá para indicar si la clave se muestra o no
  idUser:number;   // Almacena el id del usuario que inició la sesión
  
  constructor(  private fb:FormBuilder, private service:LoginService, private router:Router ) {
    this.crearFormulario();
  }
  
  ngOnInit(): void {
  }

  // Gerena un formulario reactivo al inicializarse el componente (Reactive Form)
  crearFormulario(){
    this.forma = this.fb.group({
      correo : ['rodrigog_safi@latmobile.com', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')] ],
      clave  : ['123456', [ Validators.required, Validators.minLength(3) ]  ],
    });
  }

  // Conecta al endpoint y obtenemos las credenciales validadas del usuario
  login(){
    // Comprobamos si el form es inválido
    if ( this.forma.valid ) {
      // Variables que contendrán el correo y clave del usuario para ser comprobadas
      const correo:string = this.forma.get('correo').value;
      const password:string = this.forma.get('clave').value;

      // Alerta para notificar que estamos por consultar sus datos
      Swal.fire({
        icon: 'info',
        title: 'Espera un momento :)',
        text: 'Estamos procesando tu información...',
        allowOutsideClick: false,
        showConfirmButton: false,
        timer: 1300
      }).then(() => {
        // Llamada al servicio
        this.service.getRes(correo,password).subscribe( (res:LoginCode)=>{
          // Si los datos son válidos, muestra el modal, sino muestra un mensaje y limpia el form
          if (res.errorCode==1 && res.success) {
            // console.log(res.message);
            // Modal para ingresar el código que llega al correo
            Swal.fire({
              text: 'Agrega el codigo enviado a tu correo',
              input: 'text',
              inputAttributes:{
                require: 'true'
              },
              confirmButtonText: 'Empezar',
              allowOutsideClick: false,
              
            }).then((result) => {
              if (result['isConfirmed']){
                this.service.signInConfirm(correo, password, result.value).subscribe( (resp:VerifyCodeResponse)=>{
                  // console.log(resp.data[0].id);
                  this.idUser = resp.data[0].id;
                  // Enviamos el id de quien inicia la sesion al servicio
                  this.service.getSesionId(this.idUser);
                  // Optimizar la respuesta del arreglo
                  const bearer:string = resp.data[0].accessToken; // Contiene el token del usuario
                  if ( resp ) {
                    localStorage.setItem( 'bearer', bearer);
                    this.router.navigate(["/dashboard"]);
                  }else{
                    Swal.fire({
                      icon: 'error',
                      title: 'Código Inválido',
                      text: 'Favor ingresa y comprueba nuevamente tu información',
                      confirmButtonText: 'Entendido',
                      allowOutsideClick: false
                    })
                  }
                });
              }
            });
          }else{
            // console.log(res.errorCode);
            Swal.fire({
              icon: 'error',
              title: 'Datos Inválidos',
              text: 'Favor ingresa y comprueba nuevamente tu información',
              confirmButtonText: 'Entendido',
              allowOutsideClick: false
            })
          }
        })
      }) //
    }
  }

  // Show/Hide pass
  password() {
    this.show = !this.show;
  }

}
