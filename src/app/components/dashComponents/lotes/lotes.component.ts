import { Component, OnInit } from '@angular/core';
import { VerifyCodeResponse } from 'src/app/interfaces/login.interface';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lotes',
  templateUrl: './lotes.component.html',
  styleUrls: ['./lotes.component.css']
})
export class LotesComponent implements OnInit {

  lotes:any[] = []; // Arreglo de lotes (No estan tipados)
  idLote:number;

  constructor( private dashService:DashboardService ) { }

  ngOnInit(): void {
    this.getLotes();
  }
  
  // Metodo para obtener todos los lotes
  getLotes(){
    this.dashService.getLotes().subscribe( (res:VerifyCodeResponse)=>{
      this.lotes = res.data;
      // console.log(this.lotes);
    })
  }

  // Metodo para eliminar lotes
  deleteLotes(id:number, nombre:string){
    Swal.fire({
      icon: 'question',
      title: 'Eliminar Subasta',
      text: '¿Desea eliminar el lote '+nombre+'?',
      confirmButtonText: 'Eliminar',
      confirmButtonColor: '#d33',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      cancelButtonColor: '#aaa',
      allowOutsideClick: false
    }).then( result=>{
      if (result.isConfirmed) {
        this.dashService.deleteLote(id).subscribe( (res:VerifyCodeResponse)=>{
          this.getLotes(); // Refrescamos los datos luego de eliminar
          Swal.fire({
            icon: 'success',
            title: 'Hecho',
            text: res.message,
            showConfirmButton: false,
            showCancelButton: false,
            allowOutsideClick: false,
            timer: 1300
          })
        })
      }
    })
  }

}
