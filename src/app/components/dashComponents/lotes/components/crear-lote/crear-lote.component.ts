import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-crear-lote',
  templateUrl: './crear-lote.component.html',
  styleUrls: ['./crear-lote.component.css']
})
export class CrearLoteComponent implements OnInit {

  forma:FormGroup;

  constructor( private fb:FormBuilder ) {
    this.crearForm();
  }

  ngOnInit(): void {
  }

  crearForm(){
    this.forma = this.fb.group({
      nombre : ['', [ Validators.required, Validators.minLength(5) ] ],
      valor : ['', [ Validators.required, Validators.minLength(5) ] ],
      peso : ['', [ Validators.required ] ],
      medidaAlto : ['', [ Validators.required ] ],
      medidaAncho : ['', [ Validators.required ] ],
      descripcion : ['', [ Validators.required ] ],
    })
  }

  // Metodo para agregar lotes
  agregar(){
    console.log(this.forma.value)
  }

}
