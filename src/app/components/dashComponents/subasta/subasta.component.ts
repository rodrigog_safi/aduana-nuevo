import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Subasta, VerifyCodeResponse } from 'src/app/interfaces/login.interface';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-subasta',
  templateUrl: './subasta.component.html',
  styleUrls: ['./subasta.component.css']
})
export class SubastaComponent implements OnInit {

  subastas:Subasta[] = []; // Arreglo de subastas

  constructor( private dashService:DashboardService ) { }

  ngOnInit(): void {
    this.getSubastas();
  }

  // Metodo para obtener todas las subastas
  getSubastas(){
    this.dashService.getSubasta()
    // Los filtramos por medio de pipe, para al final solo obtener la data de la respuesta
    .pipe( map( (usuarios:VerifyCodeResponse) => {
      if (usuarios.errorCode==1) {
        return  usuarios['data'];
      }
      return usuarios;
    }))
    .subscribe( (res:any) => {
      this.subastas = res;
      // console.log(this.subastas);
      return res;
    })
  }

  // Metodo para eliminar subastas
  deleteSubasta( id:number, nombre:string ){
    Swal.fire({
      icon: 'question',
      title: 'Eliminar Subasta',
      text: '¿Desea eliminar la subasta '+nombre+'?',
      confirmButtonText: 'Eliminar',
      confirmButtonColor: '#d33',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      cancelButtonColor: '#aaa',
      allowOutsideClick: false
    }).then( result=>{
      if (result.isConfirmed) {
        this.dashService.deleteSubasta(id).subscribe( (res:VerifyCodeResponse)=>{
          // console.log(res.message);
          this.getSubastas(); // Refrescamos los datos que obtenemos
          Swal.fire({
            icon: 'success',
            title: 'Hecho',
            text: res.message,
            showConfirmButton: false,
            showCancelButton: false,
            allowOutsideClick: false,
            timer: 1200
          })
        })
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Hecho',
          text: 'Accion cancelada',
          showConfirmButton: false,
          showCancelButton: false,
          allowOutsideClick: false,
          timer: 1200
        })
      }
    })
  }

}