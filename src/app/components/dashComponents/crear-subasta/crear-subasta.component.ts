import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/internal/operators/map';
import { Subasta, VerifyCodeResponse } from 'src/app/interfaces/login.interface';
import { DashboardService } from 'src/app/services/dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear-subasta',
  templateUrl: './crear-subasta.component.html',
  styleUrls: ['./crear-subasta.component.css']
})
export class CrearSubastaComponent implements OnInit {

  forma:FormGroup; // Reactive form
  fechaFormato:string="YYYY-MM-ddTHH:mm:ss.SSSz+00:00"; // El formato aun no es el de la db

  idSubasta:number; // Almacena el id que se envia por la url al pulsar el boton editar
  idUrl:string;
  subastas:Subasta[] = [];

  json:any;

  // Body que mandamos a la peticion post de agregar en el servicio
  body:any={
    nombre: '',
    descripcion: '',
    fecha_inicio: '',
    fecha_fin: '',
    hora_inicio: '',
    hora_fin: '',
    hora_inicio_defecto: '',
    fechaSubInicio: '',
    fechaSubFin: '',
    horaInicioSub: '',
    horaFinSub: '',
    activo: true,
    delete: false,
    idSubasta: '',
  };

  constructor( private fb:FormBuilder, private dashService:DashboardService,
               private route:ActivatedRoute, private router:Router ) {
    this.crearFormulario();
  }

  ngOnInit(): void {
    // Si viene un id por medio de la url, lo almacenamos en la variable e inmediatamente se determina
    // que el usuario ha elegido editar algun registro ya existente
    this.idUrl = this.route.snapshot.paramMap.get('id');
    if (this.idUrl!==null) { // Si es null, entonces el componente se usa para crear registros
      this.idSubasta = parseInt( this.route.snapshot.paramMap.get('id') );
      this.getSubastaById(this.idSubasta);
    }
  }

  crearFormulario(){
    this.forma = this.fb.group({
      nombre : ['', [ Validators.required, Validators.minLength(5) ] ],
      descripcion : ['', [ Validators.required, Validators.minLength(5) ] ],
      fecha_inicio : ['', [ Validators.required ] ],
      fecha_fin : ['', [ Validators.required ] ],
      hora_inicio : ['', [ Validators.required ] ],
      hora_fin : ['', [ Validators.required ] ],
      hora_inicio_defecto : ['', [ Validators.required ] ],
      fechaSubInicio : ['', [ Validators.required ] ],
      fechaSubFin : ['', [ Validators.required ] ],
      horaInicioSub : ['', [ Validators.required ] ],
      horaFinSub : ['', [ Validators.required ] ],
    });
  }

  // Transformando la data que se almacena en el body que mandamos al servicio para agregar
  preAgregar(){
    if (this.idUrl!==null) {
      this.body.nombre = this.forma.controls['nombre'].value;
      this.body.descripcion = this.forma.controls['descripcion'].value;
      this.body.fecha_inicio = formatDate(this.forma.controls['fecha_inicio'].value, this.fechaFormato, 'en-US');
      this.body.fecha_fin = formatDate(this.forma.controls['fecha_fin'].value, this.fechaFormato, 'en-US');
      this.body.hora_inicio = formatDate(this.forma.controls['hora_inicio'].value, this.fechaFormato, 'en-US');
      this.body.hora_fin = formatDate(this.forma.controls['hora_fin'].value, this.fechaFormato, 'en-US');
      this.body.hora_inicio_defecto = formatDate(this.forma.controls['hora_inicio_defecto'].value, this.fechaFormato, 'en-US');
      this.body.fechaSubInicio = formatDate(this.forma.controls['fechaSubInicio'].value, this.fechaFormato, 'en-US');
      this.body.fechaSubFin = formatDate(this.forma.controls['fechaSubFin'].value, this.fechaFormato, 'en-US');
      this.body.horaInicioSub = formatDate(this.forma.controls['horaInicioSub'].value, this.fechaFormato, 'en-US');
      this.body.horaFinSub = formatDate(this.forma.controls['horaFinSub'].value, this.fechaFormato, 'en-US');
      this.body.activo = true;
      this.body.idSubasta = this.idUrl;
    }else{
      this.body.nombre = this.forma.controls['nombre'].value,
      this.body.descripcion = this.forma.controls['descripcion'].value,
      this.body.fecha_inicio = formatDate(this.forma.controls['fecha_inicio'].value, this.fechaFormato, 'en-US'),
      this.body.fecha_fin = formatDate(this.forma.controls['fecha_fin'].value, this.fechaFormato, 'en-US'),
      this.body.hora_inicio = formatDate(this.forma.controls['hora_inicio'].value, this.fechaFormato, 'en-US'),
      this.body.hora_fin = formatDate(this.forma.controls['hora_fin'].value, this.fechaFormato, 'en-US'),
      this.body.hora_inicio_defecto = formatDate(this.forma.controls['hora_inicio_defecto'].value, this.fechaFormato, 'en-US'),
      this.body.fechaSubInicio = formatDate(this.forma.controls['fechaSubInicio'].value, this.fechaFormato, 'en-US'),
      this.body.fechaSubFin = formatDate(this.forma.controls['fechaSubFin'].value, this.fechaFormato, 'en-US'),
      this.body.horaInicioSub = formatDate(this.forma.controls['horaInicioSub'].value, this.fechaFormato, 'en-US'),
      this.body.horaFinSub = formatDate(this.forma.controls['horaFinSub'].value, this.fechaFormato, 'en-US'),
      this.body.activo = true
      this.body.delete = false
    }
  }

  // Mandando a agregar la nueva subasta
  agregar(){
    this.preAgregar();
    // console.log( this.body );
    this.dashService.createSubasta(this.body).subscribe( (res:VerifyCodeResponse)=>{
      // console.log(res.message);
      Swal.fire({
        icon: 'success',
        title: 'Hecho',
        text: res.message,
        showConfirmButton: false,
        showCancelButton: false,
        allowOutsideClick: false,
        timer: 1300
      })
    })
  }


  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------Funciones de Actualizar Subasta-----------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//


  // Metodo para obtener toda la informacion de una subasta por medio de su ID
  getSubastaById( id:number ){
    // console.log(id);
    this.dashService.getSubastaById(id)
    .pipe( map( (usuarios:VerifyCodeResponse) => {
      if (usuarios.errorCode==1) {
        return  usuarios['data'];
      }
      return usuarios;
    }))
    .subscribe( (res)=>{
      // console.log(res);
      this.body = res;
      this.subastas = this.body; // Lo convertimos a un arreglo de subastas para poder ocuparlo en la view
      // console.log(this.subastas);
      this.setValues(); // Al obtener los datos, colocamos esos valores en los inputs del form
    })
  }

  // Funcion para colocar los valores en el form, que obtenemos al consultar por id
  setValues(){
    this.forma.get('nombre').setValue(this.body[0].nombre);
    this.forma.get('descripcion').setValue(this.body[0].descripcion);
    this.body = {}; // Reiniciamos el valor de body para reestablecerle los datos antes de editar
    // this.forma.get('fecha_inicio').setValue(new Date().toISOString().substring(0,10));
    // this.forma.get('fecha_fin').setValue(new Date().toISOString().substring(0,10));
    // this.forma.get('hora_inicio').setValue(new Date().toISOString().substring(0,10));
    // this.forma.get('hora_fin').setValue(new Date().toISOString().substring(0,10));
    // this.forma.get('hora_inicio_defecto').setValue(new Date().toISOString().substring(0,10));
    // this.forma.get('fechaSubInicio').setValue(new Date().toISOString().substring(0,10));
    // this.forma.get('fechaSubFin').setValue(new Date().toISOString().substring(0,10));
    // this.forma.get('horaInicioSub').setValue(new Date().toISOString().substring(0,10));
    // this.forma.get('horaFinSub').setValue(new Date().toISOString().substring(0,10));
  }

  editSubasta(){
    this.preAgregar(); // Reutilizamos el metodo para reasignar los valores de body
    this.dashService.editSubasta( this.body ).subscribe( (res:VerifyCodeResponse)=>{
      Swal.fire({
        icon: 'success',
        title: 'Hecho',
        text: res.message,
        showConfirmButton: false,
        showCancelButton: false,
        allowOutsideClick: false,
        timer: 1300
      })
      this.router.navigate(['/dashboard/subasta']) // Regreso al usuario a la ventana principal
    })
  }

}