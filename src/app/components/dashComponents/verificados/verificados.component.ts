import { Component, OnInit } from '@angular/core';
import { map, pipe } from 'rxjs';
import { Datum, VerifiedUsersResponse } from 'src/app/interfaces/dashboard.interface';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-verificados',
  templateUrl: './verificados.component.html',
  styleUrls: ['./verificados.component.css']
})
export class VerificadosComponent implements OnInit {

  verified:boolean=true;

  constructor( private dashService:DashboardService ) { }

  users: any[] = [];  //Para usuarios verificados
  usersNo: any = [];  //Para usuarios no verificados
  user: any = {} ;    //Para obtener un usuario por medio de su id
  idUserSelected:number;  //para almacenar el id del usuario seleccionado

  ngOnInit(): void {
    this.getVerificados();
    this.getNoVerificados();
  }

  // Switch para mostrar la tabla de usuarios verificados, y visceversa
  mostrarVerificados(param:boolean){
    this.verified = param;
  }

  // Metodo para obtener usuarios verificados
  getVerificados(){
    this.dashService.getVerificados().subscribe( items=>{
      if (items.length==0) {
        this.users = items;
        console.log("Nada para mostrar");
      }else{
        this.users = items;
        // console.log(items);
      }
    })
  }

  // Metodo para obtener usuarios no verificados
  getNoVerificados(){
    this.dashService.getNoVerificados().subscribe( items=>{
      if (items.length==0) {
        this.users = items;
        console.log("Nada para mostrar");
      }else{
        this.usersNo = items;
        // console.log(this.usersNo);
      }
    })
  }

  // Metodo para obtener usuarios por medio de su id
  getById( id:number ){
    this.idUserSelected=id;  //Almacenamos el valor del id del usuario seleccionado
    this.dashService.getById(this.idUserSelected).subscribe( (items:Datum)=>{
      this.user = items[0];
      console.log(this.user);
    })
  }

  // Metodo para borrar usuarios
  deleteUser( id:number ){
    console.log(id);
  }
}
