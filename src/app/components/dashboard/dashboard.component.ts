import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css',
               '../../../assets/app-assets/vendors/css/vendors.min.css',
               '../../../assets/app-assets/css/bootstrap.css',
               '../../../assets/app-assets/css/bootstrap-extended.css',
               '../../../assets/app-assets/css/colors.css',
               '../../../assets/app-assets/css/components.css',
               '../../../assets/app-assets/css/themes/dark-layout.css',
               '../../../assets/app-assets/css/themes/semi-dark-layout.css',
               '../../../assets/app-assets/css/core/menu/menu-types/vertical-menu.css',
               '../../../assets/app-assets/assets/css/style.css' ]
})
export class DashboardComponent implements OnInit {

  idSesion:string; // Consultamos por el id de quien inicio la sesion en loginService

  constructor( private service:LoginService, private router:Router ) { }

  ngOnInit(): void {
    // Obtenemos el Id de la sesion cuando se inica el componente
    this.getIdSession();
  }

  // Obtengo el id de quien inicia la sesion para consultas futuras, y lo almaceno en localStorage
  // Incompleto porque no hay interfaz de administracion de perfil
  getIdSession(){
    this.idSesion = localStorage.getItem('SessionID');
    // console.log(this.idSesion);
  }

  // Logout limpiará todo el localStorage
  logout(){
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }

}
