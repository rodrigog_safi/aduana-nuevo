import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './components/dashboard/dashboard.component';

// Componentes internos de Dashboard
import { TranscribirComponent } from './components/dashComponents/transcribir/transcribir.component';
import { VerificadosComponent } from './components/dashComponents/verificados/verificados.component';
import { MantenimientoComponent } from './components/dashComponents/mantenimiento/mantenimiento.component';
import { LotesComponent } from './components/dashComponents/lotes/lotes.component';
import { SubastaComponent } from './components/dashComponents/subasta/subasta.component';
import { CrearSubastaComponent } from './components/dashComponents/crear-subasta/crear-subasta.component';
import { CrearLoteComponent } from './components/dashComponents/lotes/components/crear-lote/crear-lote.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    TranscribirComponent,
    VerificadosComponent,
    MantenimientoComponent,
    LotesComponent,
    SubastaComponent,
    CrearSubastaComponent,
    CrearLoteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
