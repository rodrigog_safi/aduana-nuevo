import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import Swal from 'sweetalert2';
import { LoginService } from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor( private service:LoginService, private router:Router ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      const resp = this.service.isAuth();  // Valor que obtenemos al consultar en localStorage

      // Si la respuesta es true, impide que acceda al login, y muestra una alerta que debe confirmar
      if ( resp == true) {
        // Swal.fire({
        //   icon: 'question',
        //   title: 'Aviso',
        //   text: 'Estás intentando navegar hacia el login, esto conlleva que tu sesión se cierre, ¿Deseas continuar?',
        //   confirmButtonText: 'Continuar',
        //   showCancelButton: true,
        //   cancelButtonText: 'Cancelar',
        //   allowOutsideClick: false
        // }).then( (result) =>{
        //   if (result['isConfirmed']){
        //     // Si acepta, se le corta la sesion, evitando así que se loguee 2 veces y colapse todo
        //     localStorage.clear();
        //     this.router.navigate(['/login']);
        //   }else if (result['isDismissed']) {
        //     // Si cancela, su sesión continua, evitando que sea desautenticado
        //     this.router.navigate(['/dashboard']);
        //   }
        // })
        this.router.navigate(['/dashboard']);
        return false;
      }else{
        return true;
      }
  }
  
}
