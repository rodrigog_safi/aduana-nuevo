import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  constructor( private service:LoginService, private router:Router ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      const resp = this.service.isAuth();  // Valor que obtenemos al consultar en localStorage
      // Si la respuesta es false, impide que acceda a la ruta protegida, caso contrario accede
      if ( resp == false) {
        this.router.navigate(['/login']);
        return false;
      }else{
        return true;
      }
  }
  
}