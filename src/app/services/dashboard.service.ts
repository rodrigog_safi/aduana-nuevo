import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  ruta:string = "http://api-subastas.latmobile.com:8080/api/";
  token:string; // Obtiene el token contenido en localStorage

  // Variables de Crear Subasta
  body:any={
    nombre: '',
    descripcion: '',
    fecha_inicio: '',
    fecha_fin: '',
    hora_inicio: '',
    hora_fin: '',
    hora_inicio_defecto: '',
    fechaSubInicio: '',
    fechaSubFin: '',
    horaInicioSub: '',
    horaFinSub: '',
    
    activo: true,
    delete: false,
    idSubasta: '',
  };

  constructor( private http:HttpClient ) {}

  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //--------------------------------Métodos de VerificadosComponent-------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  getVerificados(){
    this.token=localStorage.getItem('bearer');
    // console.log(this.token);
    return this.http.get( `${this.ruta}user/verificados`, { headers: new HttpHeaders().set('Authorization', `Bearer ${this.token}`)} )
      .pipe( map( usuarios => usuarios['data'] ))  //filtramos los datos por medio de un pipe
  }
  getNoVerificados(){
    this.token=localStorage.getItem('bearer');
    // console.log(this.token);
    return this.http.get( `${this.ruta}user/noverificados`, { headers: new HttpHeaders().set('Authorization', `Bearer ${this.token}`)} )
    .pipe( map( usuarios => usuarios['data'] ))    //filtramos los datos por medio de un pipe
  }

  getById( id:number ){
    this.token=localStorage.getItem('bearer');
    // console.log(this.token);
    return this.http.get( `${this.ruta}user/${id}`, { headers: new HttpHeaders().set('Authorization', `Bearer ${this.token}`)} )
      .pipe( map( usuarios => usuarios['data'] ))  //filtramos los datos por medio de un pipe
  }

  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //------------------------------------Métodos de SubastaComponent-------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//

  // Metodo para obtener todas las subastas
  getSubasta(){
    this.token=localStorage.getItem('bearer');
    return this.http.get(`${this.ruta}subasta`, { headers: new HttpHeaders().set('Authorization', `Bearer ${this.token}`)} );
  }
  
  // Metodo para crear una nueva subasta
  createSubasta( body:any ){
    this.token=localStorage.getItem('bearer');
    this.body = body;
    // console.log(this.body);
    return this.http.post(`${this.ruta}subasta`, this.body, { headers: new HttpHeaders().set('Authorization', `Bearer ${this.token}`)} );
  }
  
  // Metodo para borrar una subasta
  deleteSubasta( id:number ){
    this.token=localStorage.getItem('bearer');
    // console.log(id);
    return this.http.delete(`${this.ruta}subasta/${id}`, { headers: new HttpHeaders().set('Authorization', `Bearer ${this.token}`)} );
  }
  
  // Metodo para obtener los datos de una subasta seleccionada por medio de su ID
  getSubastaById( id:number ){
    this.token=localStorage.getItem('bearer');
    // console.log(id);
    return this.http.get(`${this.ruta}subasta/${id}`, { headers: new HttpHeaders().set('Authorization', `Bearer ${this.token}`)} );
  }

  // Metodo para editar una subasta
  editSubasta( body:any ){
    this.token=localStorage.getItem('bearer');
    // this.body = body;
    return this.http.put(`${this.ruta}subasta`, body, { headers: new HttpHeaders({'Content-Type': 'application/json'}).set('Authorization', `Bearer ${this.token}`)} );
  }

  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //-------------------------------------Métodos de LotesComponent--------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//
  //----------------------------------------------------------------------------------------------------//

  // Metodo para obtener todos los lotes
  getLotes(){
    this.token=localStorage.getItem('bearer');
    return this.http.get(`${this.ruta}lote`, { headers: new HttpHeaders().set('Authorization', `Bearer ${this.token}`)} );
  }  
  
  // Metodo para eliminar el lote seleccionado
  deleteLote(id:number){
    this.token=localStorage.getItem('bearer');
    // console.log(id);
    return this.http.delete(`${this.ruta}lote/${id}`, { headers: new HttpHeaders().set('Authorization', `Bearer ${this.token}`)} );
  }
}
