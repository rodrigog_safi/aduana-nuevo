import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  ruta:string = "http://api-subastas.latmobile.com:8080/api/auth";
  token:string;
  idSesion:number;  // Contiene el id de quien inicio la sesion

  constructor( private http:HttpClient ) {
    // this.isAuth();
  }

  // Metodo para comprobar los datos primarios del usuario
  getRes( correo:string, pass:string ) {
    return this.http.post(`${this.ruta}/signincode`, { correo: correo, password: pass });
  }

  // Metodo para cuando el usuario introduce el codigo de confirmacion y se debe comprobar
  signInConfirm( correo:string, password:string, codigo:number ){
    return this.http.post(`${this.ruta}/signin`, { correo: correo, password: password, codigo: codigo });
  }

  // Método del AuthGuard que comprueba si existe un token antes de acceder a una ruta protegida
  isAuth(){
    this.token=localStorage.getItem('bearer');
    // console.log(this.token);
    if (this.token == null) {
      return false;
    }else{
      return true
    }
  }

  //Recibiendo el id de quien inicia la sesion
  getSesionId(id:number){
    if (id!==null) {
      // console.log(id);
      this.idSesion = id;
      localStorage.setItem('SessionID', `${this.idSesion}`);
      return id;
    }else{
      return null;
    }
  }
  
}
