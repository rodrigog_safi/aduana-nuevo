import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MantenimientoComponent } from './components/dashComponents/mantenimiento/mantenimiento.component';
import { LotesComponent } from './components/dashComponents/lotes/lotes.component';
import { TranscribirComponent } from './components/dashComponents/transcribir/transcribir.component';
import { VerificadosComponent } from './components/dashComponents/verificados/verificados.component';
import { LoginComponent } from "./components/login/login.component";
import { AuthGuard } from './guards/auth.guard'; // Guard para todas las demás rutas
import { LoginGuard } from './guards/login.guard'; // Guard para el login
import { SubastaComponent } from './components/dashComponents/subasta/subasta.component';
import { CrearSubastaComponent } from './components/dashComponents/crear-subasta/crear-subasta.component';
import { CrearLoteComponent } from './components/dashComponents/lotes/components/crear-lote/crear-lote.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { 
    path: 'dashboard',
    component: DashboardComponent, canActivate: [ AuthGuard ],
    children: [
      { path: 'users', component: VerificadosComponent, canActivate: [ AuthGuard ] },
      { path: 'transcribir', component: TranscribirComponent, canActivate: [ AuthGuard ] },
      { path: 'mantenimiento', component: MantenimientoComponent, canActivate: [ AuthGuard ] },
      { path: 'lotes', component: LotesComponent, canActivate: [ AuthGuard ] },
      { path: 'subasta', component: SubastaComponent, canActivate: [ AuthGuard ] },
      { path: 'subasta/crear', component: CrearSubastaComponent, canActivate: [AuthGuard] },
      { path: 'subasta/editar/:id', component: CrearSubastaComponent, canActivate: [AuthGuard] },
      { path: 'lotes/crear', component: CrearLoteComponent, canActivate: [ AuthGuard ] },
      { path: '**', pathMatch: 'full', redirectTo: 'users' },
    ]
  },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
