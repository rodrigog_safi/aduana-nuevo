export interface VerifiedUsersResponse {
    success:   boolean;
    errorCode: number;
    data:      Datum[];
    message:   string;
}

export interface Datum {
    idUsuario:     number;
    nombre:        string;
    direccion:     string;
    municipio:     string;
    departamento:  Departamento;
    dui:           string;
    nit:           string;
    telefono:      string;
    correo:        string;
    estadoUsuario: EstadoUsuario;
    rol:           Rol;
}

export enum Departamento {
    LaLibertad = "La Libertad",
    SANSalvador = "San Salvador",
    SantaAna = "Santa Ana",
}

export interface EstadoUsuario {
    idEstadoUsuario: number;
    nombre:          EstadoUsuarioNombre;
    descripcion:     EstadoUsuarioDescripcion;
    activo:          boolean;
}

export enum EstadoUsuarioDescripcion {
    EstadosParaLosUsuariosQueYaFueronVerificados = "Estados para los usuarios que ya fueron verificados",
}

export enum EstadoUsuarioNombre {
    Verificado = "VERIFICADO",
}

export interface Rol {
    idRol:       number;
    nombre:      RolNombre;
    descripcion: RolDescripcion;
    activo:      number;
}

export enum RolDescripcion {
    RolParaLosUsuariosAdministradores = "Rol para los usuarios administradores",
    RolParaLosUsuariosQueHaranOfertas = "Rol para los usuarios que haran ofertas",
}

export enum RolNombre {
    Admin = "ADMIN",
    Usuario = "USUARIO",
}
