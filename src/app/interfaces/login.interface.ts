export interface LoginCode {
    success:   boolean;
    errorCode: number;
    data:      any[];
    message:   string;
}

// Tipado del codigo de respuesta que se recibe de la api
export interface VerifyCodeResponse {
    success:   boolean;
    errorCode: number;
    data:      Datum[];
    message:   string;
}

export interface Datum {
    id:          number;
    email:       string;
    roles:       string[];
    accessToken: string;
    tokenType:   string;
}

// Tipado de la data de subastas
export interface Subasta {
    idSubasta:              number;
    nombre:                 string;
    descripcion:            string;
    horaInicio:             Date;
    fechaInicio:            Date;
    horaFin:                Date;
    horaDefecto:            Date;
    fechaFin:               Date;
    fechaInicioSuscripcion: Date;
    fechaFinSuscripcion:    Date;
    horaInicioSuscripcion:  Date;
    horaFinSuscripcion:     Date;
    activo:                 boolean;
    delete:                 boolean;
}
